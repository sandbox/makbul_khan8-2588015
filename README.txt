README.txt
==========

mvaayoo
A module providing an integration between the mVaayoo SMS service and
the SMS framework module using exposed API by mVaayoo SMS gateway API.

CONFIGURATION:
======================

admin/config/mvaayoo

INSTRUCTIONS
======================
1. Download and enable the module from module via /admin/modules or
through drush command: drush en mvaayoo
2. To you mVaayoo service you need to register on their site. Create your account at below URL.
http://www.mvaayoo.com/UI/Registration.aspx
3. After creating your account you will receive an sms / email on your registered details with "USERID" and "PASSWORD". 
4. Click on configure link from /admin/modules for mVaayoo configuration page.
(admin/config/mvaayoo/settings)

AUTHOR/MAINTAINER
======================
-<kadam DOT monika1206@gmail DOT com>

